module.exports = {
  "extends": "stylelint-config-standard",
  "rules": {
    "indentation": 2,
    "declaration-colon-newline-after": null,
    "block-closing-brace-newline-after": null,
    "value-list-comma-newline-after": null,
    "no-empty-source": null,
    "no-descending-specificity": null,
    "at-rule-empty-line-before": null,
    "at-rule-no-unknown": [
      true,
      {
        "ignoreAtRules": [
          "extend",
          "at-root",
          "debug",
          "warn",
          "error",
          "use",
          "forward",
          "if",
          "else",
          "for",
          "each",
          "while",
          "mixin",
          "include",
          "content",
          "return",
          "tailwind",
          "apply",
          "responsive",
          "variants",
          "screen",
          "function"
        ]
      }
    ]
  }
};
