let mix = require('laravel-mix');
require('laravel-mix-stylelint');
mix
  .setPublicPath('dist')
  .sass('igniter.scss', 'dist', {
  })
  .stylelint({
    context: './',
    failOnError: false,
    fix: true,
  })
  .sourceMaps();
mix.sass('docs.scss', 'dist')
