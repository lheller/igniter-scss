# Igniter - SCSS Starter Pack

Igniter is a collection of usefull utility and helper classes, styling resets and mixins, inspired by some popular css frameworks.
It includes some concepts of ulitity-first frameworks, a few components like a component based framework but tries to make everything optional to measily customizable and reduce unnecessary overhead code as much as possible.

This package does not include a component or utility class to fit every single possible usecase, just for the point of having them all covered.
It is not meant to be a full out-of-box css framework or a complete replacement for any further css files - it's a **starter pack** that lays a practical foundation within a SCSS based theming workflow.

## Documentation:

https://lheller.gitlab.io/igniter-scss/
